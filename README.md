# README #

(Quick Summary: Copy data from file A to file B byte by byte. Repeat with different size files, and different size transfer blocks.)

For different file sizes 1MB - 128MB, doubling the file size in each step, record the time your program requires to complete the file copy when using read() and write() system calls. Generate a graph that depicts the program performance. Repeat this experiment, but instead of copying individual bytes, use larger size portions of the file that are copied in each read() and write() system  call. For instance you may choose to copy the file in chunks of 2,4,8,...1024 byte units. Generate a performance graph and interpret your experimental results.

### What is this repository for? ###

* CSCE 4600
* Homework 1
* Question 3

### How do I get set up? ###

* Compile with `gcc copyByte.c`
* Run with `./a.out folderName outputFile`
* Example: If there is a folder "files" containing all input files, the program can be run with `./a.out files output.csv` and the output can be viewed in output.csv
* The outputFile is stored as a comma separated value file in the directory of the executable.

### Input File Naming ###

* Input files are the size of the file in MB in a .txt document
* The program only supports 1,2,4,8,16,32,64,128 MB file sizes
* Input file can be generated with `dd if=/dev/zero of=$SIZE.txt  bs=1M  count=$SIZE`
* All files should be stored in the folderName specified as a command line argument

### Who do I talk to? ###

* Cassie Chin
* cassiechin9793@gmail.com