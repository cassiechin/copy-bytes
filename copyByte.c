/*
 * Cassie Chin
 * CSCE 4600, Homework 1, Question 3
 *
 * Copy data from file A to file B byte by byte.
 * Repeat with differnet size files, and different size transfer blocks.
 *
 ***********************************************************************
 *
 * For different file sizes 1MB - 128MB, doubling the file size in each
 * step, record the time your program requires to complete the file copy
 * when using read() and write() system calls. Generate a graph that
 * depicts the program performance.
 *
 * Repeat this experiment, but instead of copying individual bytes, use
 * larger size portions of the file that are copied in each read() and
 * write() system  call. For instance you may choose to copy the file in
 * chunks of 2,4,8,...1024 byte units. Generate a performance graph and
 * interpret your experimental results.
 *
 ***********************************************************************
 *
 * Input files are generated with:
 * dd if=/dev/zero of=output.dat  bs=1M  count=24
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#define BUF_SIZE         134217728 // 128 MB
#define BYTES_PER_MB     1048576
#define MIN_FILE_SIZE_MB 1
#define MAX_FILE_SIZE_MB 128
#define MIN_FILE_SIZE_B  MIN_FILE_SIZE_MB*BYTES_PER_MB
#define MAX_FILE_SIZE_B  MAX_FILE_SIZE_MB*BYTES_PER_MB

/*
 * Copy input file into the temp.txt file
 * @param fd A file descriptor of the file to copy
 * @param sizeOfFile The size of the input file
 * @param sizeOfBlock The amount of data to copy of at one time
 * @return The total time it took to copy the file
 */
float copyFile (int fd, int sizeOfFile, int sizeOfBlock) {
  // Copy the input file into this file
  int fdTemp = open("/tmp/temp.txt", O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP |S_IROTH );
  if (!fdTemp) return 0;

  size_t bytesToRead = sizeOfBlock;
  char *buf = (char *) calloc (BUF_SIZE, sizeof(char));

  // Copy the file in chunks
  clock_t startTime = clock();
  while (read(fd, buf, bytesToRead)) {
    write (fdTemp, buf, bytesToRead);
  }
  clock_t endTime = clock();
  free (buf);
  close (fdTemp);

  // Return the total time it took for all read and write operations.
  float diff = (((float)endTime - (float)startTime) / 1000000.0F ) * 1000;
  return diff;
}

/*
 * Call the copy function for each file on block sizes from 1B - 1MB
 * @param argv[1]The folder containing the input files.
 * The files must be named X.txt where X is the size of
 * the file in MB
 * @param argv[2]The output file to save the times to
 */
int main (int argc, char** argv) {
  if (argc != 3) {
    printf("Usage: %s inputFolderPath outputFileName\n", argv[0]);
    return 0;
  }

  FILE *f_output = fopen (argv[2], "w");
  if (!f_output) return 0;

  char *fileName = (char *) calloc (64, sizeof(char));
  strcpy(fileName, argv[1]);

  // Add '/' to the folder path
  if (fileName[strlen(fileName)-1] != '/') {
    fileName[strlen(fileName)] = '/';
  }

  int blockSize = 1;
  int fileSize = 1;

  // Do the experiment for all the files
  for (fileSize = MIN_FILE_SIZE_B; fileSize <= MAX_FILE_SIZE_B; fileSize *= 2) {
    // generate input file name
    sprintf(fileName, "%s%d.txt", fileName, fileSize/BYTES_PER_MB);

    // run on all block sizes up to 1 MB
    for (blockSize = 1; blockSize <= BYTES_PER_MB; blockSize *= 2) {
      // time how long it takes to copy a this file with this blocksize
      int fd = open (fileName, O_RDONLY);
      if (!fd) {
	printf("Check README for input file naming.");
	printf("File %s not found\nAborting...\n", fileName);
	return 0;
      }

      float time = copyFile (fd, fileSize, blockSize);
      printf("%d, %d, %f\n", fileSize, blockSize, time);
      fprintf(f_output, "%d, %d, %f\n", fileSize, blockSize, time);
      close (fd);
    }
  }

  fclose(f_output);
  return 0;
}
